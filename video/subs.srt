﻿1
00:00:02,540 --> 00:00:04,900
Someone posts a photo
on the internet

2
00:00:04,900 --> 00:00:07,040
after 15 minutes we know
a lot about this person

3
00:00:07,040 --> 00:00:08,920
Including where she is

4
00:00:10,240 --> 00:00:12,480
You smoke a lot, Ada

5
00:00:12,640 --> 00:00:13,440
There you go

6
00:00:15,240 --> 00:00:17,240
Does it bother your dog, Lord?

7
00:00:17,560 --> 00:00:19,840
When you smoke cigarettes in front of him?
*Instagram photo with dogs name*

8
00:00:20,480 --> 00:00:21,440
There you go

9
00:00:21,649 --> 00:00:23,133
Really, I was walking nearby,
saw you sitting here

10
00:00:23,133 --> 00:00:26,373
And I thought I always wanted
to spare you a cigarette

11
00:00:27,141 --> 00:00:29,621
I wanted to ask you
about your Ikea bed

12
00:00:29,621 --> 00:00:30,501
Is it comfortable?

13
00:00:30,960 --> 00:00:33,000
I want to buy the same bed

14
00:00:35,822 --> 00:00:39,042
For example, when
Berta jumps on it

15
00:00:39,785 --> 00:00:43,805
You know, won't she
fall and hurt herself?

16
00:00:45,357 --> 00:00:45,917
Jowita?

17
00:00:46,616 --> 00:00:47,116
Yes

18
00:00:47,297 --> 00:00:47,797
M….?

19
00:00:47,871 --> 00:00:48,371
Yes

20
00:00:48,909 --> 00:00:49,929
Really?

21
00:00:50,449 --> 00:00:51,409
Really? Hello

22
00:00:51,590 --> 00:00:52,090
Rafał

23
00:00:53,186 --> 00:00:53,686
Hello

24
00:00:53,686 --> 00:00:54,186
Rafał

25
00:00:54,640 --> 00:00:55,140
Wow

26
00:00:56,092 --> 00:00:58,612
You are in Poland only since March?
*Facebook post with travel details"

27
00:00:59,080 --> 00:01:00,120
My name is Rafał

28
00:01:00,423 --> 00:01:00,923
Wow

29
00:01:01,442 --> 00:01:02,882
Wow, I finally met you

30
00:01:02,882 --> 00:01:03,382
Wow

31
00:01:03,513 --> 00:01:04,013
Hi

32
00:01:04,013 --> 00:01:05,471
(friend asking) But how
do you know Kinga?

33
00:01:06,184 --> 00:01:06,684
Hi

34
00:01:07,342 --> 00:01:09,022
You know what? Tell me...

35
00:01:10,140 --> 00:01:13,020
You are with Daniel a long time. right?
*FB update: in relationship with Daniel*

36
00:01:13,020 --> 00:01:15,240
What does he have that I lack?

37
00:01:16,833 --> 00:01:17,873
I don't know you

38
00:01:17,873 --> 00:01:21,017
But Kinga, tell me seriously,
because you know...

39
00:01:22,020 --> 00:01:24,000
Wait, first tell me
how do you know me

40
00:01:24,260 --> 00:01:26,060
You know what? I always
wanted to meet you

41
00:01:26,420 --> 00:01:27,180
Very much

42
00:01:27,701 --> 00:01:29,701
Did you take him to Jastarnia?

43
00:01:29,701 --> 00:01:31,240
Last week you've been to Jastarnia *Instagram
photo with geolocation of the vacation place*

44
00:01:31,380 --> 00:01:33,060
And did you take him there?

45
00:01:33,060 --> 00:01:33,860
Oh, Fuck...

46
00:01:33,860 --> 00:01:35,278
Did he enjoy himself there?

47
00:01:35,278 --> 00:01:36,680
I have german shepard too

48
00:01:36,880 --> 00:01:38,480
What did you drink right now?

49
00:01:38,823 --> 00:01:39,543
Water

50
00:01:40,158 --> 00:01:40,958
That's good

51
00:01:41,082 --> 00:01:41,722
very good

52
00:01:41,722 --> 00:01:43,875
Because if you've been
working out today

53
00:01:43,875 --> 00:01:44,782
In Calypso gym in Wilanów

54
00:01:44,782 --> 00:01:46,863
Then you should
watch your weight

55
00:01:46,863 --> 00:01:48,612
Don't indulge yourself

56
00:01:49,082 --> 00:01:51,082
First workout, then… you know

57
00:01:51,082 --> 00:01:51,582
Wow

58
00:01:52,780 --> 00:01:54,820
You work in PZU insurance company, right?
(Facebook "Job" screenshot)

59
00:01:55,200 --> 00:01:57,540
You know what? I crashed
my car on purpose

60
00:01:57,780 --> 00:01:59,360
Because I hoped
that I will see you

61
00:02:00,990 --> 00:02:02,590
And you play volleyball

62
00:02:02,835 --> 00:02:03,395
I used to

63
00:02:03,395 --> 00:02:05,545
You used too, I love
volleyball players

64
00:02:05,715 --> 00:02:08,035
They always wear
such nice outfits

65
00:02:08,326 --> 00:02:08,886
I love it

66
00:02:08,886 --> 00:02:10,475
No, really. How do you know me?

67
00:02:11,060 --> 00:02:13,660
Wait. Did you passed
your psychology exam?

68
00:02:13,820 --> 00:02:16,580
*Instagram conversation "- What
are you studying? - Psychology"*

69
00:02:16,680 --> 00:02:19,880
…after sleepless night,
you've made it, right?

70
00:02:20,100 --> 00:02:21,920
What a meeting, right?

71
00:02:23,740 --> 00:02:25,520
Unbelievable. Where
is "Fadzinka"?

72
00:02:25,700 --> 00:02:27,260
*Instagram screenshot with
dog's name as a hashtag*

73
00:02:27,260 --> 00:02:29,340
Why isn't she on
a walk with you?

74
00:02:30,604 --> 00:02:32,124
She's hanging out at home

75
00:02:32,124 --> 00:02:33,145
She's hanging out at your house?

76
00:02:33,145 --> 00:02:35,465
What is "Patello Femoral
Pain Syndrome "?

77
00:02:35,920 --> 00:02:37,600
*Instagram screenshot
of medical procedure*

78
00:02:37,600 --> 00:02:40,543
*friend asking* Where did you get
this information about Kinga?

79
00:02:40,780 --> 00:02:42,140
I really wanted to..

80
00:02:42,140 --> 00:02:42,907
I really wanted you to...

81
00:02:42,907 --> 00:02:44,255
But how do you know
this information?

82
00:02:44,255 --> 00:02:45,909
… for you to explain it to me.

83
00:02:46,280 --> 00:02:49,000
Listen. I know that you
are a little dirty

84
00:02:49,042 --> 00:02:51,202
That cake on bachelorette party

85
00:02:51,202 --> 00:02:52,402
With this black...

86
00:02:55,920 --> 00:02:58,100
PS4 gift, really nice.
You like it

87
00:02:58,340 --> 00:03:00,980
You've bought additional
gamepad, right?

88
00:03:04,988 --> 00:03:06,348
Does it...

89
00:03:06,348 --> 00:03:08,748
Listen. Does it suprise you?

90
00:03:09,400 --> 00:03:12,060
I found all this information
about you on the internet

91
00:03:12,200 --> 00:03:14,902
It was enough that you
posted a photo of a beer

92
00:03:14,902 --> 00:03:16,482
and your friends hangbag

93
00:03:16,600 --> 00:03:18,920
and I can tell you all
this information about you

94
00:03:19,121 --> 00:03:19,621
So...

95
00:03:19,621 --> 00:03:21,521
I don't know if it's
responsible behaviour

96
00:03:22,098 --> 00:03:22,938
Wait.

97
00:03:22,938 --> 00:03:25,419
You' ve found her here only
from this Instagram photo?

98
00:03:25,535 --> 00:03:26,035
Yes

99
00:03:26,440 --> 00:03:28,621
So… You didn't talk
to Ada before?

100
00:03:28,621 --> 00:03:31,337
No. I didn't know her.
We meet first time ever

101
00:03:31,337 --> 00:03:32,477
Ada, you can confirm?

102
00:03:32,999 --> 00:03:34,359
First time in my life

103
00:03:37,260 --> 00:03:39,020
Now, there is a question for you

104
00:03:39,920 --> 00:03:41,900
Do you feel safe with it?

105
00:03:41,909 --> 00:03:42,409
Yes, I do

106
00:03:42,409 --> 00:03:44,049
I am nice, friendly guy

107
00:03:44,896 --> 00:03:46,576
All the people that I know

108
00:03:46,922 --> 00:03:48,602
I recognize immediately

109
00:03:48,851 --> 00:03:50,691
Because I'm good with faces

110
00:03:51,020 --> 00:03:51,520
But...

111
00:03:51,644 --> 00:03:52,764
Realize this...

112
00:03:52,764 --> 00:03:54,137
We don't know each other

113
00:03:54,137 --> 00:03:55,355
And I know almost
everything about you

114
00:03:55,620 --> 00:03:58,520
Not everything, but some really
personal details of your life

115
00:03:59,080 --> 00:04:00,680
You know. We were...

116
00:04:01,241 --> 00:04:02,601
2 kilometers away

117
00:04:02,601 --> 00:04:03,386
We saw your post

118
00:04:03,462 --> 00:04:05,062
We are strangers and...

119
00:04:05,062 --> 00:04:06,460
look how much we know about you

120
00:04:07,340 --> 00:04:11,000
Aren't you afraid that people
know too much about you?

121
00:04:12,747 --> 00:04:13,247
So?

122
00:04:13,414 --> 00:04:15,634
I think I do now

123
00:04:16,840 --> 00:04:20,908
Moments after we finished the conversations, two of the
girls set their social media profiles to "private"

